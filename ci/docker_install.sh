#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq

apt-get install wget

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need


apt-get update -y && apt-get install -y libpng-dev

#apt-get update && apt-get install -y zlib1g-dev 

docker-php-ext-install pdo_mysql

#docker-php-ext-install mbstring

#docker-php-ext-install zip

docker-php-ext-install gd
