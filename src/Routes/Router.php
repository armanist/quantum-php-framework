<?php

/**
 * Quantum PHP Framework
 *
 * An open source software development framework for PHP
 *
 * @package Quantum
 * @author Arman Ag. <arman.ag@softberg.org>
 * @copyright Copyright (c) 2018 Softberg LLC (https://softberg.org)
 * @link http://quantum.softberg.org/
 * @since 1.0.0
 */

namespace Quantum\Routes;

use Quantum\Exceptions\RouteException;
use Quantum\Exceptions\ExceptionMessages;
use Quantum\Hooks\HookManager;
use Quantum\Hooks\HookController;
use Quantum\Http\Request;

/**
 * Router Class
 *
 * Router class parses URIS and determine routing
 *
 * @package Quantum
 * @subpackage Routes
 * @category Routes
 */
class Router extends RouteController
{

    private $request;

    /**
     * List of routes
     *
     * @var array
     */
    public $routes = [];

    /**
     * @var array
     */
    private $matchedUris = [];

    /**
     * @var array
     */
    private $routesGroups = [];

    /**
     * @var string
     */
    private $requestUri = '';

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Find Route
     *
     * Matches any routes that may exists in config/routes.php file of specific module
     * against the URI to determine current route and current module
     *
     * @return void
     * @throws RouteException When repetitive route was found
     */
    public function findRoute()
    {
        $this->findStraightMatches();

        if (!$this->matchedUris) {
            $this->findPatternMatches();
        }

        if (!$this->matchedUris) {
            self::$currentRoute = null;
            HookManager::call('pageNotFound');
        }

        if ($this->matchedUris) {
            if (count($this->routesGroups)) {

                $this->routesGroups[0]['uri'] = $this->request->getCurrentUri();

                self::$currentModule = $this->routesGroups[0]['module'];

                for ($i = 0; $i < count($this->routesGroups) - 1; $i++) {
                    for ($j = $i + 1; $j < count($this->routesGroups); $j++) {
                        if ($this->routesGroups[$i]['method'] == $this->routesGroups[$j]['method']) {
                            self::$currentRoute = null;
                            throw new RouteException(_message(ExceptionMessages::REPETITIVE_ROUTE_SAME_METHOD, $this->routesGroups[$j]['method']));
                            break 2;
                        }
                        if ($this->routesGroups[$i]['module'] != $this->routesGroups[$j]['module']) {
                            self::$currentRoute = null;
                            throw new RouteException(ExceptionMessages::REPETITIVE_ROUTE_DIFFERENT_MODULES);
                            break 2;
                        }
                    }
                }

                foreach ($this->routesGroups as $route) {
                    if (strpos($route['method'], '|') !== false) {
                        if (in_array($this->request->getMethod(), explode('|', $route['method']))) {
                            self::$currentRoute = $route;
                            break;
                        }
                    } else if ($this->request->getMethod() == $route['method']) {
                        self::$currentRoute = $route;
                        break;
                    }
                }
            } else {
                self::$currentModule = $this->routesGroups[0]['module'];
                self::$currentRoute = $this->routesGroups[0];
            }
        }

        HookManager::call('handleHeaders');

        if ($this->request->getMethod() != 'OPTIONS') {
            $this->checkMethod();
        }
    }

    /**
     * Set Routes
     * 
     * @param array $routes
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
    }

    /**
     * Get Routes
     * 
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Finds straight matches
     * 
     * @return void
     */
    private function findStraightMatches()
    {
        $requestUri = trim(urldecode(preg_replace('/[?]/', '', $this->request->getCurrentUri())), '/');

        foreach ($this->routes as $route) {
            if ($requestUri == trim($route['route'], '/')) {
                $route['args'] = [];
                $this->matchedUris[] = $route['route'];
                $this->routesGroups[] = $route;
            }
        }
    }

    /**
     * Finds matches by pattern
     * 
     * @return void
     */
    private function findPatternMatches()
    {
        $requestUri = urldecode(parse_url($this->request->getCurrentUri())['path']);

        foreach ($this->routes as $route) {
            $pattern = trim($route['route'], '/');
            $pattern = str_replace('/', '\/', $pattern);
            $pattern = preg_replace_callback('/(\\\\\/)*\[(:num)(:([0-9]+))*\](\?)?/', array($this, 'getPattern'), $pattern);
            $pattern = preg_replace_callback('/(\\\\\/)*\[(:alpha)(:([0-9]+))*\](\?)?/', array($this, 'getPattern'), $pattern);
            $pattern = preg_replace_callback('/(\\\\\/)*\[(:any)(:([0-9]+))*\](\?)?/', array($this, 'getPattern'), $pattern);

            $pattern = mb_substr($pattern, 0, 4) != '(\/)' ? '(\/)?' . $pattern : $pattern;

            preg_match("/^" . $pattern . "$/u", $requestUri, $matches);

            if (count($matches)) {
                $this->matchedUris = $matches[0] ?: '/';
                array_shift($matches);
                $route['args'] = array_diff($matches, ['', '/']);
                $route['pattern'] = $pattern;
                $this->routesGroups[] = $route;
            }
        }
    }

    /**
     * Check Method
     * 
     * Matches the http method defined in config/routes.php file of specific module
     * against request method to determine current route
     *
     * @throws RouteException When Http method is other then defined in config/routes.php of specific module
     */
    private function checkMethod()
    {
        if (strpos(self::$currentRoute['method'], '|') !== false) {
            if (!in_array($this->request->getMethod(), explode('|', self::$currentRoute['method']))) {
                throw new RouteException(_message(ExceptionMessages::INCORRECT_METHOD, $this->request->getMethod()));
            }
        } else if ($this->request->getMethod() != self::$currentRoute['method']) {
            throw new RouteException(_message(ExceptionMessages::INCORRECT_METHOD, $this->request->getMethod()));
        }
    }

    /**
     * Finds url pattern
     *
     * @param string $matches
     * @return string
     */
    private function getPattern($matches)
    {
        $replacement = '';

        if (isset($matches[5]) && $matches[5] == '?') {
            $replacement .= '(\/)?';
        } else {
            $replacement .= '(\/)';
        }

        switch ($matches[2]) {
            case ':num':
                $replacement .= '([0-9]';
                break;
            case ':alpha':
                $replacement .= '([a-zA-Z]';
                break;
            case ':any':
                $replacement .= '([^\/]';
                break;
        }

        if (isset($matches[4]) && is_numeric($matches[4])) {
            if (isset($matches[5]) && $matches[5] == '?') {
                $replacement .= '{0,' . $matches[4] . '})';
            } else {
                $replacement .= '{' . $matches[4] . '})';
            }
        } else {
            if (isset($matches[5]) && $matches[5] == '?') {
                $replacement .= '*)';
            } else {
                $replacement .= '+)';
            }
        }

        return $replacement;
    }

}
