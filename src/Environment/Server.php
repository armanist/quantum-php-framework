<?php

/**
 * Quantum PHP Framework
 *
 * An open source software development framework for PHP
 *
 * @package Quantum
 * @author Arman Ag. <arman.ag@softberg.org>
 * @copyright Copyright (c) 2018 Softberg LLC (https://softberg.org)
 * @link http://quantum.softberg.org/
 * @since 2.0.0
 */

namespace Quantum\Environment;

/**
 * Class Server
 * @package Quantum\Environment
 */
class Server
{

    private $server = [];

    public function __construct()
    {
        $this->server = $_SERVER ?? [];
    }

    public function uri(): ?string
    {
        return $this->server['REQUEST_URI'] ?? null;
    }

    public function url(): string
    {
        return (isset($this->server['HTTPS']) ? 'https' : 'http') . '://' .
                $this->server['HTTP_HOST'] . '/' .
                ($this->server['REQUEST_URI'] ?? '') .
                ($this->server['QUERY_STRING'] ?? '');
    }

    public function query(): ?string
    {
        return $this->server['QUERY_STRING'] ?? null;
    }

    public function referrer(): ?string
    {
        return $this->server['HTTP_REFERER'] ?? null;
    }

    public function method(): ?string
    {
        return $this->server['REQUEST_METHOD'] ?? null;
    }

    public function contentType(): ?string
    {
        return $this->server['CONTENT_TYPE'] ?? null;
    }

    public function ajax(): bool
    {
        if (!empty($this->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        }

        return false;
    }

}
